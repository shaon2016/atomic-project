<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/5/2016
 * Time: 6:38 PM
 */

namespace App\BITM\SEIP128330\BookTitle;


class BookTitle
{
    public $id;
    private $title;
    private $created;
    private $modified;
    private $createdBy;
    private $modifiedBy;
    private $deletedAt;

    function __construct($bookTitle) {

    }

    function index() {

        return " I am listing data ";
    }

    public function create() {
        return " I am create form ";

    }

    function store() {
        return " I am storing data ";
    }

    function edit() {
        return "I am editing form ";

    }

    function update() {
        return "I am updating data ";
    }

    function delete() {
        return " I delete data";

    }

    function view() {

    }

}